# Aditya Dhulipala
# Python Practice

"""Visualizing Twitter Sentiment Across America"""

from data import word_sentiments, load_tweets
from datetime import datetime
from doctest import run_docstring_examples
from geo import us_states, geo_distance, make_position, longitude, latitude
from maps import draw_state, draw_name, draw_dot, wait, message, save_to_image
from string import ascii_letters
from ucb import main, trace, interact, log_current_line


# Phase 1: The Feelings in Tweets

def make_tweet(text, time, lat, lon):
    """Return a tweet, represented as a python dictionary.

    text  -- A string; the text of the tweet, all in lowercase
    time  -- A datetime object; the time that the tweet was posted
    lat   -- A number; the latitude of the tweet's location
    lon   -- A number; the longitude of the tweet's location

    >>> t = make_tweet("just ate lunch", datetime(2012, 9, 24, 13), 38, 74)
    >>> tweet_words(t)
    ['just', 'ate', 'lunch']
    >>> tweet_time(t)
    datetime.datetime(2012, 9, 24, 13, 0)
    >>> p = tweet_location(t)
    >>> latitude(p)
    38
    """
    return {'text': text, 'time': time, 'latitude': lat, 'longitude': lon}

def tweet_words(tweet):
    """Return a list of the words in the text of a tweet."""
    return extract_words(tweet.get('text'))

def tweet_time(tweet):
    """Return the datetime that represents when the tweet was posted."""
    return tweet.get('time')

def tweet_location(tweet):
    """Return a position (see geo.py) that represents the tweet's location."""
    return make_position(tweet.get('latitude'), tweet.get('longitude'))

def tweet_string(tweet):
    """Return a string representing the tweet."""
    return '"{0}" @ {1}'.format(tweet['text'], tweet_location(tweet))

def extract_words(text):
    """Return the words in a tweet, not including punctuation.

    >>> extract_words('anything else.....not my job')
    ['anything', 'else', 'not', 'my', 'job']
    >>> extract_words('i love my job. #winning')
    ['i', 'love', 'my', 'job', 'winning']
    >>> extract_words('make justin # 1 by tweeting #vma #justinbieber :)')
    ['make', 'justin', 'by', 'tweeting', 'vma', 'justinbieber']
    >>> extract_words("paperclips! they're so awesome, cool, & useful!")
    ['paperclips', 'they', 're', 'so', 'awesome', 'cool', 'useful']
    >>> extract_words('@(cat$.on^#$my&@keyboard***@#*')
    ['cat', 'on', 'my', 'keyboard']
    """
    # text1 used for testing purpose
    # text1 = '@(cat$.on^#$my&@keyboard***@#* some text here'

    # Approach: Replace every special char with ' '
    #           call split() on final string
    for character in text:
        if character not in ascii_letters:
            text = text.replace(character, ' ')
    return text.split()

def make_sentiment(value):
    """Return a sentiment, which represents a value that may not exist.

    >>> positive = make_sentiment(0.2)
    >>> neutral = make_sentiment(0)
    >>> unknown = make_sentiment(None)
    >>> has_sentiment(positive)
    True
    >>> has_sentiment(neutral)
    True
    >>> has_sentiment(unknown)
    False
    >>> sentiment_value(positive)
    0.2
    >>> sentiment_value(neutral)
    0
    """
    assert value is None or (value >= -1 and value <= 1), 'Illegal value'
    return (value,)

def has_sentiment(s):
    """Return whether sentiment s has a value."""
    return s != (None,)

def sentiment_value(s):
    """Return the value of a sentiment s."""
    assert has_sentiment(s), 'No sentiment value'
    return s[0]

def get_word_sentiment(word):
    """Return a sentiment representing the degree of positive or negative
    feeling in the given word.

    >>> sentiment_value(get_word_sentiment('good'))
    0.875
    >>> sentiment_value(get_word_sentiment('bad'))
    -0.625
    >>> sentiment_value(get_word_sentiment('winning'))
    0.5
    >>> has_sentiment(get_word_sentiment('Berkeley'))
    False
    """
    # Learn more: http://docs.python.org/3/library/stdtypes.html#dict.get
    return make_sentiment(word_sentiments.get(word))

#### Test Data t1, t2, t3
##t1 = make_tweet('i love my job. #winning', None, 0, 0)
##t2 = make_tweet("saying, 'i hate my job'", None, 0, 0)
##t3 = make_tweet("berkeley golden bears!", None, 0, 0)

def analyze_tweet_sentiment(tweet):
    """ Return a sentiment representing the degree of positive or negative
    sentiment in the given tweet, averaging over all the words in the tweet
    that have a sentiment value.

    If no words in the tweet have a sentiment value, return
    make_sentiment(None).

    >>> positive = make_tweet('i love my job. #winning', None, 0, 0)
    >>> round(sentiment_value(analyze_tweet_sentiment(positive)), 5)
    0.29167
    >>> negative = make_tweet("saying, 'i hate my job'", None, 0, 0)
    >>> sentiment_value(analyze_tweet_sentiment(negative))
    -0.25
    >>> no_sentiment = make_tweet("berkeley golden bears!", None, 0, 0)
    >>> has_sentiment(analyze_tweet_sentiment(no_sentiment))
    False
    """
    # Approach: Iterate over words in the tweet
    #           keeping track of each word's sentiment value
    #           Sum the list of sentiments and divide by
    #           the total num of words to get average sentiment value
    
    sentiment_values_list = [] # initiaise empty list of sentiment values
    average = make_sentiment(None)

    for each_word in tweet_words(tweet):
        current_word = get_word_sentiment(each_word)
        if has_sentiment(current_word):
            sentiment_values_list.append(sentiment_value(current_word))

    sum_of_terms = sum(sentiment_values_list)
    num_of_terms = len(sentiment_values_list)

    try:
        average = make_sentiment(sum_of_terms / num_of_terms)
    except ZeroDivisionError:
        pass

    return average

# Phase 2: The Geometry of Maps

def find_centroid(polygon): # IDEA: try to vectorise using numpy module --AD
    """Find the centroid of a polygon.

    http://en.wikipedia.org/wiki/Centroid#Centroid_of_polygon

    polygon -- A list of positions, in which the first and last are the same

    Returns: 3 numbers; centroid latitude, centroid longitude, and polygon area

    Hint: If a polygon has 0 area, use the latitude and longitude of its first
    position as its centroid.

    >>> p1, p2, p3 = make_position(1, 2), make_position(3, 4), make_position(5, 0)
    >>> triangle = [p1, p2, p3, p1]  # First vertex is also the last vertex
    >>> find_centroid(triangle)
    (3.0, 2.0, 6.0)
    >>> find_centroid([p1, p3, p2, p1])
    (3.0, 2.0, 6.0)
    >>> tuple(map(float, find_centroid([p1, p2, p1])))  # A zero-area polygon
    (1.0, 2.0, 0.0)
    """

    # Find Area; need to find better impl --AD
    diff_of_prod = [] # Holds the list: xi*yi+1 - xi+1*yi for i = 1 to n
    for index, value in enumerate(polygon):
        current_pos = polygon[index]
        next_pos = polygon[(index + 1) % len(polygon)]
        diff_of_prod.append(
            latitude(current_pos) * longitude(next_pos)
            - latitude(next_pos) * longitude(current_pos)
            )
    area = (1.0 / 2.0) * sum (diff_of_prod)

    # Find co-ordinates of Centroid
    x_terms = [] # Holds the list: (xi+xi+1) * (xi*yi+1 - xi+1*yi) for i = 1 to n
    y_terms = [] # Holds the list: (yi+yi+1) * (xi*yi+1 - xi+1*yi) for i = 1 to n

    for index, value in enumerate(polygon):
        current_pos = polygon[index]
        next_pos = polygon[(index + 1) % len(polygon)]

        xy_prod_first_part = latitude(current_pos) * longitude(next_pos)
        xy_prod_second_part = latitude(next_pos) * longitude(current_pos)
        xy_prod = xy_prod_first_part - xy_prod_second_part
        x_sum = latitude(current_pos) + latitude(next_pos)
        y_sum = longitude(current_pos) + longitude(next_pos)

        x_terms.append(x_sum * xy_prod)
        y_terms.append(y_sum * xy_prod)

    try:
        centroid_latitude = (1.0 / (6 * area)) * sum (x_terms)
        centroid_longitude = (1.0 / (6 * area)) * sum (y_terms)
    except ZeroDivisionError:
        centroid_latitude = latitude(polygon[0])
        centroid_longitude = longitude(polygon[0])
    
    return (centroid_latitude, centroid_longitude, abs(area))

def find_center(polygons):
    # TODO: Find a more elegant method to solve --AD
    #       HINT: Use numpy module to vectorise
    """Compute the geographic center of a state, averaged over its polygons.

    The center is the average position of centroids of the polygons in polygons,
    weighted by the area of those polygons.

    Arguments:
    polygons -- a list of polygons

    >>> ca = find_center(us_states['CA'])  # California
    >>> round(latitude(ca), 5)
    37.25389
    >>> round(longitude(ca), 5)
    -119.61439

    >>> hi = find_center(us_states['HI'])  # Hawaii
    >>> round(latitude(hi), 5)
    20.1489
    >>> round(longitude(hi), 5)
    -156.21763
    """
    # List of centroids of all polygons: [[Cx], [Cy], [Area]], where
    # [Cx] is a list of x co-ordinates of each polygon from polygons
    # [Cy] is a list of y co-ordinates of each polygon from polygons
    # [Area] is a list of areas of each polygon from polygons
    
    centroids = [[0], [0], [0]]
    X_IDX = 0; Y_IDX = 1; AREA_IDX = 2

    for each in polygons:
        centroid = list(find_centroid(each))
        try:
            centroids[X_IDX].append((centroid[X_IDX] * centroid[AREA_IDX]) / centroid[AREA_IDX])
        except ZeroDivisionError:
            pass
        try:
            centroids[Y_IDX].append((centroid[Y_IDX] * centroid[AREA_IDX]) / centroid[AREA_IDX])
        except ZeroDivisionError:
            pass
        centroids[AREA_IDX].append(centroid[AREA_IDX])

    x_times_area = [Cx * area for Cx, area in zip(centroids[X_IDX], centroids[AREA_IDX])]
    y_times_area = [Cy * area for Cy, area in zip(centroids[Y_IDX], centroids[AREA_IDX])]
    sigma_area = sum (centroids[AREA_IDX])

    try:
        center_x = sum (x_times_area) / sigma_area
    except ZeroDivisionError:
        pass
    try:
        center_y = sum (y_times_area) / sigma_area
    except ZeroDivisionError:
        pass

    return make_position(center_x, center_y)


# Phase 3: The Mood of the Nation
# Test data
us_ctrs = {n: find_center(s) for n, s in us_states.items()}
sf = make_tweet("welcome to san Francisco", None, 38, -122)
ny = make_tweet("welcome to new York", None, 41, -74)

def find_closest_state(tweet, state_centers):
    import pdb; pdb.set_trace()
    """Return the name of the state closest to the given tweet's location.

    Use the geo_distance function (already provided) to calculate distance
    in miles between two latitude-longitude positions.

    Arguments:
    tweet -- a tweet abstract data type
    state_centers -- a dictionary from state names to positions.

    >>> us_centers = {n: find_center(s) for n, s in us_states.items()}
    >>> sf = make_tweet("welcome to san Francisco", None, 38, -122)
    >>> ny = make_tweet("welcome to new York", None, 41, -74)
    >>> find_closest_state(sf, us_centers)
    'CA'
    >>> find_closest_state(ny, us_centers)
    'NJ'
    """
    # Appraoch
    # Find the distance to all states from using tweet location
    # Store them in a dictionary with associated state names
    # Return key of smallest value of those distances
    distances = {state: geo_distance(tweet_location(tweet), center) for state, center in state_centers.items()}
    return min (distances, key = distances.get)


def group_tweets_by_state(tweets):
    """Return a dictionary that aggregates tweets by their nearest state center.

    The keys of the returned dictionary are state names, and the values are
    lists of tweets that appear closer to that state center than any other.

    tweets -- a sequence of tweet abstract data types

    >>> sf = make_tweet("welcome to san francisco", None, 38, -122)
    >>> ny = make_tweet("welcome to new york", None, 41, -74)
    >>> ca_tweets = group_tweets_by_state([sf, ny])['CA']
    >>> tweet_string(ca_tweets[0])
    '"welcome to san francisco" @ (38, -122)'
    """
    tweets_by_state = {}
    us_centers = {n: find_center(s) for n, s in us_states.items()}

    # Approach: Iterate over tweets and find closest state.
    #           if state existe in the 'tweets_by_state' dictionary,
    #           then append the tweet to its 'value' list
    #           else add the state to the dictionary and add the tweet

    # TODO: Find better impl

    for each_tweet in tweets:
        state = find_closest_state(each_tweet, us_centers)
        try:
            tweets_by_state[state].append(each_tweet)
        except KeyError:
            tweets_by_state[state] = [each_tweet]        
    return tweets_by_state

### Test data
### A list of tweets containing term
##soup_tweets = load_tweets(make_tweet, 'soup')
def most_talkative_state(term):
    """Return the state that has the largest number of tweets containing term.

    If multiple states tie for the most talkative, return any of them.

    >>> most_talkative_state('texas')
    'TX'
    >>> most_talkative_state('soup')
    'CA'
    """
    tweets = load_tweets(make_tweet, term)  # A list of tweets containing term
    "*** YOUR CODE HERE ***"
    # Idea
    # for x in dic: dic[x] = len(dic[x])
    # max (dic, key = dic.get)

    # tweets_and_states is a dictionary containing
    # all the tweets according to states
    # i.e. keys = state name ('CA', 'TX', etc)
    #      values = tweets from that state
    tweets_and_states = group_tweets_by_state(tweets)

    # Now replace all the tweets with
    # a value indicating the number of
    # tweets from that state
    for state in tweets_and_states:
        tweets_and_states[state] = len (tweets_and_states[state])

    return max (tweets_and_states, key = tweets_and_states.get)

def average_sentiments(tweets_by_state):
    """Calculate the average sentiment of the states by averaging over all
    the tweets from each state. Return the result as a dictionary from state
    names to average sentiment values (numbers).

    If a state has no tweets with sentiment values, leave it out of the
    dictionary entirely.  Do NOT include states with no tweets, or with tweets
    that have no sentiment, as 0.  0 represents neutral sentiment, not unknown
    sentiment.

    tweets_by_state -- A dictionary from state names to lists of tweets
    """
    averaged_state_sentiments = {}
    for each_state in tweets_by_state:
        ls = []
        for each_tweet in tweets_by_state[each_state]:
            tweet_senti = analyze_tweet_sentiment(each_tweet)
            if has_sentiment(tweet_senti):
                ls.append(sentiment_value(tweet_senti))
        try:
            averaged_state_sentiments[each_state] = sum(ls) / len(ls)
        except ZeroDivisionError:
            pass
    return averaged_state_sentiments


# Phase 4: Into the Fourth Dimension
t1 = make_tweet("Hello this is an awesome and brilliant tweet",
               datetime(2013,9,24, 21,47,59), 38, -77)
t2 = make_tweet("Hello this is an awful and sad tweet",
               datetime(2013,9,24, 13,33,11), 41, -81)
t3 = make_tweet("this is a tweet",
               datetime(2013,9,24, 5,13,5), 38, 74)

def group_tweets_by_hour(tweets):
    """Return a dictionary that groups tweets by the hour they were posted.

    The keys of the returned dictionary are the integers 0 through 23.

    The values are lists of tweets, where tweets_by_hour[i] is the list of all
    tweets that were posted between hour i and hour i + 1. Hour 0 refers to
    midnight, while hour 23 refers to 11:00PM.

    To get started, read the Python Library documentation for datetime objects:
    http://docs.python.org/py3k/library/datetime.html#datetime.datetime

    tweets -- A list of tweets to be grouped

    >>> tweets = load_tweets(make_tweet, 'party')
    >>> tweets_by_hour = group_tweets_by_hour(tweets)
    >>> for hour in [0, 5, 9, 17, 23]:
    ...     current_tweets = tweets_by_hour.get(hour, [])
    ...     tweets_by_state = group_tweets_by_state(current_tweets)
    ...     state_sentiments = average_sentiments(tweets_by_state)
    ...     print('HOUR:', hour)
    ...     for state in ['CA', 'FL', 'DC', 'MO', 'NY']:
    ...         if state in state_sentiments.keys():
    ...             print(state, ":", round(state_sentiments[state], 5))
    HOUR: 0
    CA : 0.08333
    FL : -0.09635
    DC : 0.01736
    MO : -0.11979
    NY : -0.15
    HOUR: 5
    CA : 0.00945
    FL : -0.0651
    DC : 0.03906
    MO : 0.1875
    NY : -0.04688
    HOUR: 9
    CA : 0.10417
    NY : 0.25
    HOUR: 17
    CA : 0.09808
    FL : 0.0875
    MO : -0.1875
    NY : 0.14583
    HOUR: 23
    CA : -0.10729
    FL : 0.01667
    DC : -0.3
    MO : -0.0625
    NY : 0.21875
    """
    tweets_by_hour = {}
    tweets_by_hour = {hour:list() for hour in range(24)}

    for each_tweet in tweets:
        hour = int(tweet_time(each_tweet).strftime("%H"))
        tweets_by_hour[hour].append(each_tweet)
    
    return tweets_by_hour


# Interaction.  You don't need to read this section of the program.

def print_sentiment(text='Are you virtuous or verminous?'):
    """Print the words in text, annotated by their sentiment scores."""
    words = extract_words(text.lower())
    layout = '{0:>' + str(len(max(words, key=len))) + '}: {1:+}'
    for word in words:
        s = get_word_sentiment(word)
        if has_sentiment(s):
            print(layout.format(word, sentiment_value(s)))

def draw_centered_map(center_state='TX', n=10):
    """Draw the n states closest to center_state."""
    us_centers = {n: find_center(s) for n, s in us_states.items()}
    center = us_centers[center_state.upper()]
    dist_from_center = lambda name: geo_distance(center, us_centers[name])
    for name in sorted(us_states.keys(), key=dist_from_center)[:int(n)]:
        draw_state(us_states[name])
        draw_name(name, us_centers[name])
    draw_dot(center, 1, 10)  # Mark the center state with a red dot
    wait()

def draw_state_sentiments(state_sentiments):
    """Draw all U.S. states in colors corresponding to their sentiment value.

    Unknown state names are ignored; states without values are colored grey.

    state_sentiments -- A dictionary from state strings to sentiment values
    """
    for name, shapes in us_states.items():
        sentiment = state_sentiments.get(name, None)
        draw_state(shapes, sentiment)
    for name, shapes in us_states.items():
        center = find_center(shapes)
        if center is not None:
            draw_name(name, center)

def draw_map_for_term(term='my job'):
    """Draw the sentiment map corresponding to the tweets that contain term.

    Some term suggestions:
    New York, Texas, sandwich, my life, justinbieber
    """
    tweets = load_tweets(make_tweet, term)
    tweets_by_state = group_tweets_by_state(tweets)
    state_sentiments = average_sentiments(tweets_by_state)
    draw_state_sentiments(state_sentiments)
    for tweet in tweets:
        s = analyze_tweet_sentiment(tweet)
        if has_sentiment(s):
            draw_dot(tweet_location(tweet), sentiment_value(s))
    save_to_image()
    wait()

def draw_map_by_hour(term='my job', pause=0.0):
    """Draw the sentiment map for tweets that match term, for each hour."""
    tweets = load_tweets(make_tweet, term)
    tweets_by_hour = group_tweets_by_hour(tweets)

    for hour in range(24):
        current_tweets = tweets_by_hour.get(hour, [])
        tweets_by_state = group_tweets_by_state(current_tweets)
        state_sentiments = average_sentiments(tweets_by_state)
        draw_state_sentiments(state_sentiments)
        message("{0:02}:00-{0:02}:59".format(hour))
        # AD: Custom changes begin
        for tweet in current_tweets:
            s = analyze_tweet_sentiment(tweet)
            if has_sentiment(s):
                draw_dot(tweet_location(tweet), sentiment_value(s))
        # AD Custome changes end
        wait(pause)

def run_doctests(names):
    """Run verbose doctests for all functions in space-separated names."""
    g = globals()
    errors = []
    for name in names.split():
        if name not in g:
            print("No function named " + name)
        else:
            run_docstring_examples(g[name], g, True, name)

@main
def run(*args):
    """Read command-line arguments and calls corresponding functions."""
    import argparse
    parser = argparse.ArgumentParser(description="Run Trends")
    parser.add_argument('--print_sentiment', '-p', action='store_true')
    parser.add_argument('--run_doctests', '-t', action='store_true')
    parser.add_argument('--draw_centered_map', '-d', action='store_true')
    parser.add_argument('--draw_map_for_term', '-m', action='store_true')
    parser.add_argument('--draw_map_by_hour', '-b', action='store_true')
    parser.add_argument('text', metavar='T', type=str, nargs='*',
                        help='Text to process')
    args = parser.parse_args()
    for name, execute in args.__dict__.items():
        if name != 'text' and execute:
            globals()[name](' '.join(args.text))

